<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view ('register');
    }

   

    public function kirim(Request $request)
    {
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];

        return view('welcome1',['first_name' => $first_name, 'last_name' => $last_name]);
    }
}
